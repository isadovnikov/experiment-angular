interface Person {
  personId: string;
  firstName: string;
  lastName: string;
  createdAt: string | null;
}

export default Person;
