import gql from 'graphql-tag';

export default gql`
  mutation createPerson($personId: ID!, $firstName: String!, $lastName: String!, $createdAt: String!) {
    createPerson(personId: $personId, firstName: $firstName, lastName: $lastName, createdAt: $createdAt){
      __typename
      personId
      createdAt
      firstName
      lastName
    }
  }
`;
