import gql from 'graphql-tag';

export default gql`
query getAllPersons {
  allPersons {
    __typename
    personId
    firstName
    lastName
  }
}`;
