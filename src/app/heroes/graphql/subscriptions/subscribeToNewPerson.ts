import gql from 'graphql-tag';

export default gql`
subscription subscribeToNewPerson {
  subscribeToNewPerson {
    __typename
    personId
    firstName
    lastName
  }
}`;
