import {Component, OnInit} from '@angular/core';
import Person from '../types/person';
import createPerson from '../graphql/mutations/createPerson';
import * as _ from 'lodash';
import {v4 as uuid} from 'uuid';
import {AppsyncService} from '../../appsync.service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {
  person: Person;

  constructor(private appsync: AppsyncService) {
  }

  ngOnInit() {
    this.person = <Person>{};
  }

  submit() {
    this.onCreatePerson();
  }

  onCreatePerson() {
    const id = `${new Date().toISOString()}_${uuid()}`;
    // const person: Person = {
    //   personId: id,
    //   firstName: 'testFirst ' + id,
    //   lastName: 'testLast ' + id,
    //   createdAt: id,
    // };
    this.person.personId = id;
    this.person.createdAt = id;
    console.log('new person', this.person);
    this.appsync.hc().then(client => {
      client.mutate({
        mutation: createPerson,
        variables: this.person,

        optimisticResponse: () => ({
          createPerson: {
            ...this.person,
            __typename: 'Person'
          }
        }),

      }).then(({data}) => {
        console.log('mutation complete', data);
      }).catch(err => console.log('Error creating person', err));
    });
  }
}
