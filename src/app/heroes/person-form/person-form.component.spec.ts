import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PersonFormComponent} from './person-form.component';
import {FormsModule} from '@angular/forms';
import {AppsyncService} from '../../appsync.service';

describe('PersonFormComponent', () => {
  let component: PersonFormComponent;
  let fixture: ComponentFixture<PersonFormComponent>;
  let appsyncService: AppsyncService;

  beforeEach(async(() => {

    const spyAppsync = jasmine.createSpyObj('AppsyncService', ['hc']);

    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [PersonFormComponent],
      providers: [
        {provide: AppsyncService, useValue: spyAppsync}
      ]
    }).compileComponents();
    appsyncService = TestBed.get(AppsyncService);
    appsyncService.hc.and.returnValue({});
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('click button', () => {
    fixture.nativeElement.querySelector('button').click();
    expect(appsyncService.hc).toHaveBeenCalledTimes(1);
  });
});

describe('PersonFormComponent unit tests', () => {
  let personFormSpy: PersonFormComponent;
  beforeEach(async(() => {
    personFormSpy = new PersonFormComponent(null);
    // personFormSpy = jasmine.createSpyObj('PersonFormComponent', []);
  }));

  it('submit() call createPerson()', () => {
    spyOn(personFormSpy, 'onCreatePerson').and.stub();
    personFormSpy.submit();
    // todo igor decide how we want to check this
    expect(personFormSpy.onCreatePerson).toHaveBeenCalled();
    expect(personFormSpy.onCreatePerson).toHaveBeenCalledTimes(1);
  });
});
