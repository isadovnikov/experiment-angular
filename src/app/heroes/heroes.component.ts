import {Component, OnInit} from '@angular/core';
import {Hero} from '../hero';
import {AppsyncService} from '../appsync.service';
import {v4 as uuid} from 'uuid';
import Person from './types/person';
import createPerson from './graphql/mutations/createPerson';
import getAllPersons from './graphql/queries/getAllPersons';
import * as _ from 'lodash';
import subscribeToNewPerson from './graphql/subscriptions/subscribeToNewPerson';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  };
  persons: Person[];
  private no_person: boolean;
  showPersonForm: boolean;

  constructor(private appsync: AppsyncService) {
  }

  ngOnInit() {
  }

  onCreatePerson() {
    const id = `${new Date().toISOString()}_${uuid()}`;
    const person: Person = {
      personId: id,
      firstName: 'testFirst ' + id,
      lastName: 'testLast ' + id,
      createdAt: id,
    };
    console.log('new person', person);
    this.appsync.hc().then(client => {
      client.mutate({
        mutation: createPerson,
        variables: person,

        optimisticResponse: () => ({
          createPerson: {
            ...person,
            __typename: 'Person'
          }
        }),

      }).then(({data}) => {
        console.log('mutation complete', data);
      }).catch(err => console.log('Error creating person', err));
    });
  }

  getAllPersons() {
    this.appsync.hc().then(client => {
      const observable = client.watchQuery({
        query: getAllPersons,
        fetchPolicy: 'cache-and-network'
      });

      observable.subscribe(({data}) => {
        if (!data) {
          return console.log('getAllPersons - no data');
        }
        this.persons = _(data.allPersons).value();
        console.log('getAllPersons - Got data', this.persons);
        this.no_person = (this.persons.length === 0);
      });

      observable.subscribeToMore({
        document: subscribeToNewPerson,
        updateQuery: (prev: Person, {subscriptionData: {data: {subscribeToNewPerson: person}}}) => {
          console.log('updateQuery on person subscription', person, prev);
          return getAllPersons;
        }
      });
    });
  }

  clickAddPerson() {
    this.showPersonForm = true;
  }
}
