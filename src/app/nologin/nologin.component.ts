import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AmplifyService} from 'aws-amplify-angular';

@Component({
  selector: 'no-login',
  templateUrl: './nologin.component.html'
})
export class NoLoginComponent {
}
