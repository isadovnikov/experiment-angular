import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms'; // <-- NgModel lives here

import {AppComponent} from './app.component';
import {HeroesComponent} from './heroes/heroes.component';
import aws_exports from '../aws-exports';
import {Amplify} from 'aws-amplify/lib/Common';
import {AmplifyAngularModule, AmplifyService} from 'aws-amplify-angular';
import {Subject} from 'rxjs/Subject';
import {AppsyncService} from './appsync.service';
import {NavComponent} from './nav/nav.component';
import {NoLoginComponent} from './nologin/nologin.component';
import {PersonFormComponent} from './heroes/person-form/person-form.component';
import {RoutingModule} from './routing.module';
import {AuthGuard} from './auth-guard.service';

Amplify.configure(aws_exports);

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    NavComponent,
    NoLoginComponent,
    PersonFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AmplifyAngularModule,
    RoutingModule
  ],
  providers: [
    AmplifyService,
    AppsyncService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
