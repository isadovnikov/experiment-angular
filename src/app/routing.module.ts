import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HeroesComponent} from './heroes/heroes.component';
import {AppComponent} from './app.component';
import {NoLoginComponent} from './nologin/nologin.component';
import {AuthGuard} from './auth-guard.service';

const routes: Routes = [
  {path: '', component: NoLoginComponent},
  {path: 'chat', component: HeroesComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
